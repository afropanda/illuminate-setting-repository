/**
 * Class ${NAME}
 *
 * @author Allen Lai <alai@illuminateed.net>
 * @copyright Copyright ${YEAR}, Illuminate Education
#if (${NAMESPACE}) * @package ${NAMESPACE}
#end
 */
